//sample.js

//클래스 선언
class Estimate {
    //생성자()(constructor()) => 초기값 지정
    constructor(unit){
        //생성자의 필드
        this.unit = unit;
    }
    //메소드
    getEstimate(unitType, width, height){
        let priceInfo = this.unit.find(item => item.type == unitType); //(item) => {item.type == unitType}
        //this.unit => 배열(unitInfo)
        //find() => 배열에 있는 요소 만큼 돌면서 ()조건에 맞는 값을 반환(foreach와 비슷)
        return priceInfo.price * width * height;
    }
    addUnit(unit){
        this.unit.push(unit); 
        //push() => 배열에 요소 추가
    }
}
let unitInfo = [{type:'wood', price:100}, {type:'iron', price:300},{type:'plastic', price:200}]
const estimator = new Estimate(unitInfo); //객체만듦
estimator.addUnit({type:'ceramic', price:400}) //세라믹정보 추가
let result = estimator.getEstimate('ceramic', 20, 30);
console.log('결과값은 '+result);

//클래스를 쓰지 않을때(기존의 방식) == 이것도 기억
let esti = { //인스턴스
    unit : [], //속성
    getEstimate: function(unitType, width, height){ //(속성인데 함수가 왔다면)메소드
        let priceInfo = this.unit.find(item => item.type == unitType); 
        return priceInfo.price * width * height;
    },
    addUnit: function(unit){
        this.unit.push(unit);
    }
}; 
esti.unit = unitInfo;
result = esti.getEstimate('wood', 20, 30);
console.log('결과값은 '+result);
//let esti2 = {};  => 객체를 하나 더 만들고 싶으면 똑같은 기능을 다시 넣어줘야한다. 
//                    클래스를 쓰면 => 생성자 선언으로 똑같은걸 여러개 만들수 있다.
