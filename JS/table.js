//table.js
//export const table ={ ...} => table객체를 다른 js에서도 import해서 쓸수있다..
const table = { 
    //initData: [{name:'홍길동', age:20},{name:'김민수', age:22}], 
    initData: [{name:'홍길동', age:20},{name:'김민수', age:22}], 

    makeTable: function () {
        //this. => 필드 (this를 붙이면 객체범위 안에서 다 호출가능. 붙이지 않으면 함수안에서만 사용
        this.table = document.createElement('table');
        this.table.setAttribute('border', '1');
        this.makeHead();
        this.makeBody();
        
        return this.table;
    },
    makeHead: function () {
        //this => 변수선언이 아니라 객체안에 필드를 가져오는것
        this.thead = document.createElement('thead');
        let tr = document.createElement('tr');
        let fields = this.initData[0]; //대표
        for(let prop in fields){
            let th = document.createElement('th');
            th.innerText = prop.toUpperCase();
            tr.append(th);
        }    
        this.thead.append(tr);
        this.table.append(this.thead);

    },
    makeBody: function () {
        let tbody = document.createElement('tbody');
        this.initData.forEach((item) => {
            //item : {name:'홍길동', age:20}
            let tr = document.createElement('tr');
            for(let prop in item){
                let td = document.createElement('td');
                td.innerText = item[prop];
                tr.append(td);
            }
            //this.tbody.append(tr); =>함수안 this.는 윈도우 객체
            tbody.append(tr);

        });
        this.table.append(tbody);
    },
    makeTr: function () {
        
    }

    
};
//export default table;