//dom.js

//{} : 인스턴스로 만드는것? (객체(object)로 만든다)
var fruits = [];
fruits.push({
    name: '사과',
    price: 1000,
    farm: '김해농장',
    farmer: '홍길동'
});
fruits.push({
    name: '오렌지',
    price: 1500,
    farm: '성주농장',
    farmer: '김민기'
});
fruits.push({
    name: '배',
    price: 2000,
    farm: '김해농장',
    farmer: '박성윤'
});
fruits.push({
    name: '복숭아',
    price: 2500,
    farm: '성주농장',
    farmer: '최민식'
});

//배열 반복 of  ==> 대신에 foreach사용
// for(const fruits of fruits){    
//     console.log(fruits)
// }

let apple = fruits[0];
console.log(apple);
//속성값(요소) 반복 in
for (let prop in apple) {
    console.log(prop, apple[prop]);
}


function anonym() {
    console.log(this); //함수안에서의 this => 윈도우 객체
}
anonym();
console.log(this); // (아무것도 안쓰고 그냥)this=> 젤 상위인 돔 요소인 윈도우 객체 가리킴


//도큐맨트 다 읽은 후 실행하겠다는 이벤트
//(도큐맨트 => 윈도우 요소에서 화면에 보여지는 영역)
//document.addEventListener('DOMContentLoaded', createUl);

document.addEventListener('DOMContentLoaded', init);

document.addEventListener('click', function (e) { //('이벤트', 핸들러)
    e.stopPropagation(); //상위요소로 이벤트 전파 차단.(하나의 이벤트만 등록하고 싶을때)
    alert('click');
}, false); // false:하위요소 => 상위요소 이벤트 찾아감(bubbling) => 기본값
// true: 상위요소=>하위요소(capturing)  


// function createUl() {
//     var ul = document.createElement('ul');
//     fruits.forEach(function (item, idx, ary) { //배열의 경우 foreach메소드 사용 가능
//         // if (idx % 2 == 0)
//         //console.log(item);
//         var li = document.createElement('li');
//         var txtNode = document.createTextNode(item.name + ',' + item.price);
//         li.appendChild(txtNode);

//         ul.appendChild(li);
//     })

//     // console.log(typeof fruits)
//     //console.log(ul);
//     document.body.appendChild(ul);

// }

function init() {
    //input태그의 click이벤트 등록
    //console.log(document.querySelectorAll('form>input[name]')); //form 하위의 name속성이 있는 input태그 다 가져옴
    document.querySelectorAll('form>input[name]').forEach(function(item){ 
        //console.log(item); //item => input태그
        item.addEventListener('click', function(e){
            e.stopPropagation();

        });

    })

    //submit 버튼에 클릭이벤트
    document.querySelector('form>input[type=submit]').addEventListener('click', addItem);

    createTable(); //테이블 목록보여주기
}

function addItem(e){
    e.preventDefault(); //기본기능 차단.
    e.stopPropagation();
    console.log('addItem call');

    let nameVal = document.getElementById('fruit').value;
    let priceVal = document.getElementById('price').value;
    let farmVal = document.getElementById('farm').value;
    let farmerVal = document.getElementById('farmer').value;
    console.log(nameVal, priceVal, farmVal, farmerVal);

    let obj = {}; //객체선언(=new Object();)
    obj.name = nameVal; //속성추가
    obj.price = priceVal; 
    obj.farm = farmVal;
    obj.farmer = farmerVal;
    document.querySelector('tbody').appendChild(createTr(obj));

    //초기화
    document.getElementById('fruit').value = '';
    document.getElementById('price').value = '';
    document.getElementById('farm').value = '';
    document.getElementById('farmer').value = '';


}

function createTable() {

    //table의 하위에 생성
    let tableTag = document.createElement('table');
    tableTag.setAttribute('border', '1'); //속성추가

    //createHeader() 반환 값을 tableTag의 하위에 추가
    tableTag.appendChild(createHeader());
    //createBody() 반환값을 tableTag의 하위에 추가
    tableTag.appendChild(createBody());

    document.body.append(tableTag);

}

function createHeader() {
    //thead > tr > th*5 = return thead;
    let titles = ['과일', '가격', '농장', '생산자', '삭제']
    let thead = document.createElement('thead');
    let tr = document.createElement('tr');

    // let chek = document.createElement('input');
    // chek.setAttribute('type', 'checkbox');

    //체크박스 클릭하면 전체 선택
    // chek.addEventListener('click', function (e) {
    //     e.stopPropagation();
    //     if (chek.checked) {
    //         document.querySelectorAll('tbody>tr').forEach(function (item) {
    //             item.querySelector('input').checked = true;
    //         })
    //     } else {
    //         document.querySelectorAll('tbody>tr').forEach(function (item) {
    //             item.querySelector('input').checked = false;
    //         })
    //     }
    // })

    titles.forEach(function (item) {
        let th = document.createElement('th');
        th.innerText = item;
        // th.appendChild(chek);
        tr.appendChild(th);

    })

    //체크박스 추가
    let td = document.createElement('th');
    td.setAttribute('style', 'text-align: center');
    let che = document.createElement('input');
    che.setAttribute('type', 'checkbox');
    che.addEventListener('click', function(e){
        e.stopPropagation();
        let chks = document.querySelectorAll('tbody input[type=checkbox]');
        chks.forEach((item) => {  //'()=>{}' == 'function(){}'
            console.log(item);
            item.checked = che.checked;
        });
    })
    td.appendChild(che);
    tr.appendChild(td);


    thead.appendChild(tr);

    return thead; //함수를 호출한 영역으로 반환

}

function createBody() {
    let tbodyTag = document.createElement('tbody');

    //fruits배열 요소의 갯수만큼 생성.
    fruits.forEach(function (item, idx, ary) {
        tbodyTag.appendChild(createTr(item));

    });

    return tbodyTag;
}

function createTr(val = {name:'바나나', price:1000, farm:'수입', farmer:'수입이'}){  //val = {} => 값이 안들어오면 {}값을 초기값으로 쓰겠다는 의미
    let trTag = document.createElement('tr');
    
    //tr누르면 input창에 뜨도록 만들기
    trTag.addEventListener('click', showDetail);
    trTag.addEventListener('mouseover', function(){
        this.setAttribute('style','background-color: yellow');
    });
    trTag.addEventListener('mouseout', function(){
        this.removeAttribute('style','background-color');
    })

    //td 반복으로 생성.
    for (let prop in val) {
        let td = document.createElement('td');
        let txtNode = document.createTextNode(val[prop]);
        td.appendChild(txtNode);
        trTag.appendChild(td);

    }
    //버튼추가
    let td = document.createElement('td');
    let btn = document.createElement('button');
    btn.addEventListener('click', function (e) {
        e.stopPropagation(); // 상위요소 이벤트 전파 차단.(버튼 이벤트만 하겠다.=>하나의 이벤트만 등록하고 싶을때)
        console.log(this); // 이벤트핸들러 안에서의 this는 이벤트를 받는 대상 (<button>삭제</button>)
        // this.remove(); => 버튼을 지움
        this.parentElement.parentElement.remove();
    }, false); //false => 하위요소에서 상위요소 이벤트 전파
    btn.innerText = '삭제'; // = <button>삭제</button>
    td.appendChild(btn);
    trTag.appendChild(td);

    //체크박스 추가
    let td2 = document.createElement('td');
    td2.setAttribute('style', 'text-align: center');
    let che = document.createElement('input');
    che.setAttribute('type', 'checkbox');
    //che.setAttribute('checked', 'checked');
    td2.appendChild(che);
    trTag.appendChild(td2);


    return trTag;

}

function showDetail(e){
    e.stopPropagation();
    document.getElementById('fruit').value = this.children[0].innerText;
    document.getElementById('price').value = this.children[1].innerText;
    document.getElementById('farm').value = this.children[2].innerText;
    document.getElementById('farmer').value = this.children[3].innerText;

}

function delSelected(e){
    e.stopPropagation();
    //1번째 방법
    // document.querySelectorAll('tbody>tr').forEach(function(item){
    //     console.log(item);
    //     console.log(item.querySelector('input[type=checkbox]').getAttribute('checked')); //tr태그의 input창의속성 (없으면 null값)
    //     console.log(item.querySelector('input[type=checkbox]').checked); //자바스크립트 속성 => 따로 지정안해도 만들어짐
    //     if(item.querySelector('input[type=checkbox]').checked){
    //         item.remove();
    //     }

    // })

    //2번째 방법( : => 추가적 속성)
    document.querySelectorAll('tbody>tr>td>input[type=checkbox]:checked').forEach(function(item){
        console.log(item);
        item.parentElement.parentElement.remove();
    })
}


//////선택삭제 부분에 체크박스로 해놓고 그 부분 클릭하면 전체체크, 풀면 전체체크해재(checked = false -> 체크해제/ true -> 체크)


// //let delSelected = 'hhhhhh'
// let delSelected = function delSelected(){  //함수 선언식(변수를 함수로 선언)
//     console.log('hhh');
// }
// let anonymFnc = function(){  //함수표현식
//     console.log('aaaaaa')
// }
// anonymFnc();




//document.body.appendChild(ul); ->document.body가 널값 => 순서가 body읽기 전이니까 찾을 수 없다.
// <script src="dom.js"></script>를 바디태그 밑에 이동 or 이벤트('DOMContentLoaded') 등록